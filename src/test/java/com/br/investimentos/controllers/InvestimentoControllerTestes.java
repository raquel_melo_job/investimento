package com.br.investimentos.controllers;

import com.br.investimentos.enums.Risco;
import com.br.investimentos.models.Investimento;
import com.br.investimentos.models.Resultado;
import com.br.investimentos.models.Simulacao;
import com.br.investimentos.services.InvestimentoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(InvestimentoController.class)
public class InvestimentoControllerTestes {

    @MockBean
    InvestimentoService investimentoService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();

    Investimento investimento;
    Simulacao simulacao;
    Resultado resultadosimulacao;

    @BeforeEach
    public void inicializar() {
        investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("InvesTest");
        investimento.setDescricao("Teste de Investimento");
        investimento.setRisco(Risco.BAIXO);
        investimento.setPorcentagemLucro(2.0);

        simulacao = new Simulacao();
        simulacao.setDinheiroAplicado(150.00);
        simulacao.setInvestimentoId(1);
        simulacao.setMesesDeAplicacao(10);

        resultadosimulacao = new Resultado();
    }

    @Test
    public void testCriarInvestimento() throws Exception {

        Mockito.when(investimentoService.salvarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = objectMapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated()) //verifica se o status de resposta é 201 CREATED
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testBuscarTodosInvestimentos() throws Exception {
        Iterable<Investimento> investimentos = Arrays.asList(investimento);

        Mockito.when(investimentoService.buscarTodosInvestimentos()).thenReturn(investimentos);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("InvesTest")));
        // teste de erro .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("Aloooo")));
    }

   @Test
    public void testBuscarInvestimento() throws Exception {

       Optional<Investimento> investimentoOptional = Optional.of(investimento);

       Mockito.when(investimentoService.buscarPorId(Mockito.any(Integer.class))).thenReturn(investimentoOptional);

       mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/" + investimento.getId())
               .contentType(MediaType.APPLICATION_JSON))
               .andExpect(MockMvcResultMatchers.status().isOk()) //verifica se o status de resposta é 200 OK
               .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(investimento.getId())));
    }

    @Test
    public void testAtualizarInvestimento() throws Exception {
        investimento.setId(2);

        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = objectMapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/"+investimento.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk()) //verifica se o status de resposta é 200 OK
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(investimento.getId())));
    }

    @Test
    public void testSimularInvestimento() throws Exception {
        Double resultado = 0.0;

        for(int i = 0;i<simulacao.getMesesDeAplicacao();i++){
            resultado += investimento.getPorcentagemLucro()*simulacao.getDinheiroAplicado() /100;
        }
        resultado += simulacao.getDinheiroAplicado();

        Optional<Investimento> investimentoOptional = Optional.of(investimento);

        Mockito.when(investimentoService.buscarPorId(Mockito.any(Integer.class))).thenReturn(investimentoOptional);

        Mockito.when(investimentoService.calculoSimulacao(Mockito.any(Simulacao.class), Mockito.any(Investimento.class))).thenReturn(resultado);

        String json = objectMapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk()) //verifica se o status de resposta é 201 CREATED
                .andExpect(MockMvcResultMatchers.jsonPath("$.resultadoSimulado", CoreMatchers.equalTo(resultado)));

    }

    public void testDeletarInvestimento() throws Exception {
        investimentoService.deletarInvestimento(investimento);

        String json = objectMapper.writeValueAsString(investimento);

        Mockito.verify(investimentoService, Mockito.times(1)).deletarInvestimento(Mockito.any(Investimento.class));

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/"+investimento.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}
