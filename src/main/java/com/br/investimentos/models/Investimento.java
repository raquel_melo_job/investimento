package com.br.investimentos.models;

import com.br.investimentos.enums.Risco;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String nome;

    @NotNull
    private String descricao;

    @NotNull
    private Risco risco;

    @NotNull
    @DecimalMin("0.1")
    private Double porcentagemLucro;

    public Investimento() {
    }

    public Investimento(int id, String nome, String descricao, Risco risco, Double porcentagemLucro) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.risco = risco;
        this.porcentagemLucro = porcentagemLucro;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Risco getRisco() {
        return risco;
    }

    public void setRisco(Risco risco) {
        this.risco = risco;
    }

    public Double getPorcentagemLucro() {
        return porcentagemLucro;
    }

    public void setPorcentagemLucro(Double porcentagemLucro) {
        this.porcentagemLucro = porcentagemLucro;
    }
}
