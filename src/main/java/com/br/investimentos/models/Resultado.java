package com.br.investimentos.models;

public class Resultado {

    private Double resultadoSimulado;

    public Resultado() {
    }

    public Resultado(Double resultadoSimulado) {
        this.resultadoSimulado = resultadoSimulado;
    }

    public Double getResultadoSimulado() {
        return resultadoSimulado;
    }

    public void setResultadoSimulado(Double resultadoSimulado) {
        this.resultadoSimulado = resultadoSimulado;
    }
}
