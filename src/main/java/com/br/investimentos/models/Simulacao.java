package com.br.investimentos.models;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


public class Simulacao {

    @NotNull
    @NotEmpty
    private int investimentoId;

    @NotNull
    @NotEmpty
    private int mesesDeAplicacao;

    @NotNull
    @NotEmpty
    @DecimalMin(value = "100.00",message = "Valor abaixo do mínimo permitido")
    private Double dinheiroAplicado;

    public Simulacao() {
    }

    public int getInvestimentoId() {
        return investimentoId;
    }

    public void setInvestimentoId(int investimentoId) {
        this.investimentoId = investimentoId;
    }

    public int getMesesDeAplicacao() {
        return mesesDeAplicacao;
    }

    public void setMesesDeAplicacao(int mesesDeAplicacao) {
        this.mesesDeAplicacao = mesesDeAplicacao;
    }

    public Double getDinheiroAplicado() {
        return dinheiroAplicado;
    }

    public void setDinheiroAplicado(Double dinheiroAplicado) {
        this.dinheiroAplicado = dinheiroAplicado;
    }
}
