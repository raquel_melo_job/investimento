package com.br.investimentos.services;

import com.br.investimentos.models.Investimento;
import com.br.investimentos.models.Simulacao;
import com.br.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Optional<Investimento> buscarPorId(int id){
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);
        return optionalInvestimento;
    }

    public Investimento salvarInvestimento(Investimento investimento){
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public Investimento atualizarInvestimento(Investimento investimento){
        Optional<Investimento> investimentoOptional = buscarPorId(investimento.getId());
        if(investimentoOptional.isPresent()){
            Investimento investimentoData = investimentoOptional.get();

            if(investimento.getNome() == null){
                investimento.setNome(investimentoData.getNome());
            }
            if(investimento.getDescricao() == null){
                investimento.setDescricao(investimentoData.getDescricao());
            }
            if(investimento.getRisco() == null){
                investimento.setRisco(investimentoData.getRisco());
            }
            if(investimento.getPorcentagemLucro() == null){
                investimento.setPorcentagemLucro(investimentoData.getPorcentagemLucro());
            }
        }
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public void deletarInvestimento(Investimento investimento){
        investimentoRepository.delete(investimento);
    }

    public Iterable<Investimento> buscarTodosInvestimentos(){
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;
    }

    public Double calculoSimulacao(Simulacao simulacao, Investimento investimento){
        Double valorAtual = 0.0;
        for(int i = 0;i<simulacao.getMesesDeAplicacao();i++){
            valorAtual += investimento.getPorcentagemLucro()*simulacao.getDinheiroAplicado() /100;
        }
        valorAtual += simulacao.getDinheiroAplicado();
        return valorAtual;
    }
}
