package com.br.investimentos.controllers;

import com.br.investimentos.models.Investimento;
import com.br.investimentos.models.Resultado;
import com.br.investimentos.models.Simulacao;
import com.br.investimentos.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @GetMapping
    public Iterable<Investimento> buscarTodosInvestimentos(){
        return investimentoService.buscarTodosInvestimentos();
    }

    @GetMapping("/{id}")
    public Investimento buscarInvestimento(@PathVariable int id){
        Optional<Investimento> investimentoOptional = investimentoService.buscarPorId(id);
        if (investimentoOptional.isPresent()){
            return investimentoOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping()
    public ResponseEntity<Investimento> criarInvestimento(@RequestBody @Valid Investimento investimento){
        Investimento novoInvestimento = investimentoService.salvarInvestimento(investimento);
        return ResponseEntity.status(201).body(novoInvestimento);
    }

    @PutMapping("/{id}")
    public Investimento atualizarInvestimento(@PathVariable Integer id, @RequestBody Investimento investimento){
        investimento.setId(id);
        Investimento investimentoObjeto = investimentoService.atualizarInvestimento(investimento);
        return investimentoObjeto;
    }

    @PutMapping("/simulacao")
    public ResponseEntity<Resultado> simularInvestimento(@RequestBody Simulacao simulacao){
        Resultado resultadosimulacao = new Resultado();
        Investimento investimento = buscarInvestimento(simulacao.getInvestimentoId());
        resultadosimulacao.setResultadoSimulado(investimentoService.calculoSimulacao(simulacao,investimento));
        return ResponseEntity.status(200).body(resultadosimulacao);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Investimento> deletarInvestimento(@PathVariable int id){
        Optional<Investimento> investimentoOptional = investimentoService.buscarPorId(id);
        if (investimentoOptional.isPresent()){
            investimentoService.deletarInvestimento(investimentoOptional.get());
            return ResponseEntity.status(204).body(investimentoOptional.get());
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);
    }
}
