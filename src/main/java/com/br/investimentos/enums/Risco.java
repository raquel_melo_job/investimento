package com.br.investimentos.enums;

public enum Risco {

    ALTO,
    BAIXO,
    MEDIO
}
